import React from "react";
import { Link } from "react-router-dom";
import { Flex, Button, Image } from "@chakra-ui/react";

function Error() {
  return (
    <>
      <Flex width='100%' direction={"column"} justify={"center"} align={'center'}>
        <Image maxHeight={"35vh"} maxWidth={'45vh'}
          src="https://i.pinimg.com/564x/35/fe/ec/35feecb8501ba0c8c9c9baaf7881f156.jpg"
          alt="image"
        />
        <h3>Pluto says incorrect url address, but what does he know.</h3>
        <Link to={"/board"}>
          <Button>Back Home</Button>
        </Link>
      </Flex>
    </>
  );
}

export default Error;
