import React from "react";
import { Link, Outlet } from "react-router-dom";
import { Button, Flex, Image } from "@chakra-ui/react";

function NavBar() {
  return (
    <>
      <Flex
        w={"100%"}
        p={"1vw"}
        justify={"space-between"}
        align={"center"}
        boxShadow={"10px 0px 20px #888888"}
      >
        <Link to={"/board"}>
          <Button minW={"70px"} bg="#0C66E4" color={"white"} fontSize={"20px"}>
            Home
          </Button>
        </Link>
        <Button bg={"#0C66E4"}>
          <Image
            src="https://a.trellocdn.com/prgb/assets/87e1af770a49ce8e84e3.gif"
            height={{ base: "2vh", md: "2vh" }}
            p={'2px'}
            bg={"#0C66E4"}
          />
        </Button>
        <Button minW={"70px"} bg="#0C66E4" color={"white"} fontSize={"20px"}>
          User
        </Button>
      </Flex>
      <Outlet />
    </>
  );
}

export default NavBar;
