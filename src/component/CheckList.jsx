import React, { useState, useEffect } from "react";

import {
  getCheckItems,
  createCheckItem,
  deleteCheckItem,
  checkUncheckCheckItem,
} from "../services/checkItemService.cjs";

import {
  Box,
  Button,
  Flex,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
} from "@chakra-ui/react";
import { Checkbox, CheckboxGroup } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";

function EachCheckList({ cardId, checkListName, checkListId }) {
  const [allCheckItems, setAllCheckItems] = useState([]);
  const [newCheckItemName, setNewCheckItemName] = useState(`New CheckItem`);

  useEffect(() => {
    getCheckItems(checkListId).then((res) => setAllCheckItems(res));
  }, []);

  function updateNewCheckItemName(event) {
    setNewCheckItemName(
      event.target.value === "" ? `New CheckItem` : event.target.value
    );
  }

  async function createNewCheckItem() {
    const { checkItemName, checkItemId, state } = await createCheckItem(
      checkListId,
      newCheckItemName
    );
    console.log(checkItemName, checkItemId, state);
    setAllCheckItems([
      ...allCheckItems,
      {
        checkItemName: checkItemName,
        checkItemId: checkItemId,
        state: state,
      },
    ]);
    setNewCheckItemName(`New CheckItem`);
  }

  async function deleteCheckItemFunc(event, checkItemId) {
    let newArray = allCheckItems.filter(
      (checkItem) => checkItem.checkItemId !== checkItemId
    );
    setAllCheckItems(newArray);
    try {
      deleteCheckItem(checkListId, checkItemId);
    } catch (err) {
      console.log("Error! deleting checkitem: check item id:::", checkItemId);
    }
  }
  async function handleCheckBoxClick(event, itemId, state) {
    let newState = state === "incomplete" ? "complete" : "incomplete";
    let array = allCheckItems.map((item) => {
      if (item.checkItemId === itemId) {
        item.state = newState;
      }
      return item;
    });
    setAllCheckItems(array);
    try {
      checkUncheckCheckItem(cardId, checkListId, itemId, newState);
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <Accordion w={"90%"} allowToggle>
      <AccordionItem key={checkListId}>
        <h2>
          <AccordionButton>
            <Box as="span" flex="1" textAlign="left">
              {checkListName}
            </Box>
            <AccordionIcon />
          </AccordionButton>
        </h2>
        <AccordionPanel key={checkListId} pb={4}>
          <Flex flexDirection={"column"}>
            {allCheckItems.map((checkItem) => {
              return (
                <Flex key={checkItem.checkItemId}>
                  <Checkbox
                    w={"90%"}
                    isChecked={checkItem.state === "complete" ? true : false}
                    onChange={(event) =>
                      handleCheckBoxClick(
                        event,
                        checkItem.checkItemId,
                        checkItem.state
                      )
                    }
                  >
                    {checkItem.checkItemName}
                  </Checkbox>
                  <DeleteIcon
                    onClick={(event) =>
                      deleteCheckItemFunc(
                        event,
                        checkItem.checkItemId,
                        checkItem.state
                      )
                    }
                  />
                </Flex>
              );
            })}
          </Flex>
          <InputGroup p={"0"}>
            <Input
              p={"0"}
              placeholder="Add A CheckItem"
              onChange={updateNewCheckItemName}
            />
            <InputRightElement width="4.5rem">
              <Button
                h="1.75rem"
                width={"fit-content"}
                colorScheme="blue"
                mr={3}
                onClick={createNewCheckItem}
              >
                Add
              </Button>
            </InputRightElement>
          </InputGroup>
        </AccordionPanel>
      </AccordionItem>
    </Accordion>
  );
}

export default EachCheckList;
