import React, { useState, useEffect } from "react";

import MyCard from "./Card";
import {
  createCard,
  getCardsInLists,
  deleteCard,
} from "../services/cardService.cjs";

import { AddIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Card,
  Input,
  Text,
  useDisclosure,
} from "@chakra-ui/react";

function EachList({ listName, deleteListFunc, listId }) {
  const [allCards, setAllCards] = useState([]);
  const [newCardName, setNewCardName] = useState("New Card");
  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    getCardsInLists(listId).then((result) => {
      setAllCards(result);
    });
  }, []);

  async function createNewCard() {
    const { cardName, cardId } = await createCard(listId, newCardName);
    setAllCards([...allCards, { cardName: cardName, cardId: cardId }]);
    setNewCardName("New Card");
  }

  function handleCardDelete(cardId) {
    let newArray = allCards.filter((card) => card.cardId !== cardId);
    setAllCards(newArray);
    deleteCard(cardId);
  }

  return (
    <Card minW={{ base: "70vw", md: "160px" }} h={"fit-content"}>
      <Box
        display={"flex"}
        justifyContent={"space-around"}
        alignItems={"center"}
      >
        <Text
          h={"2.5rem"}
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          {listName}
        </Text>
        <DeleteIcon onClick={() => deleteListFunc(listId)}></DeleteIcon>
      </Box>
      <Accordion allowToggle>
        {allCards &&
          allCards.map((card) => {
            return (
              <MyCard
                w={"90%"}
                textAlign={"center"}
                cursor={"pointer"}
                key={card.cardId}
                onClick={onOpen}
                onOpen={onOpen}
                onClose={onClose}
                isOpen={isOpen}
                cardName={card.cardName}
                cardId={card.cardId}
                handleCardDelete={handleCardDelete}
              />
            );
          })}
        <AccordionItem bg={"lightsalmon"}>
          <h2>
            <AccordionButton>
              <Box as="span" flex="1">
                Add A Card
              </Box>
              <AddIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel w={"100%"} pb={4}>
            <Input
              placeholder="New Card Name"
              value={newCardName === "New Card" ? "" : newCardName}
              onChange={(event) => {
                setNewCardName(
                  event.target.value === "" ? "New Card" : event.target.value
                );
              }}
            />
            <Button alignSelf={"center"} onClick={createNewCard}>
              Create Card
            </Button>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </Card>
  );
}

export default EachList;
