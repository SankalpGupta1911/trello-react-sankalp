import { AddIcon } from "@chakra-ui/icons";
import {
  Button,
  Card,
  Heading,
  Input,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Portal,
} from "@chakra-ui/react";
import React from "react";

function AddAList({ newListName, storeNewListName, createNewList }) {
  return (
    <Popover>
      <PopoverTrigger closeOnBlur={true}>
        <Card
          minWidth={{ base: "160px", md: "160px" }}
          height={{ base: "8vh", md: "10vh" }}
          justify={"center"}
          align={"center"}
          cursor={"pointer"}
        >
          <Heading
            size="md"
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <AddIcon height={"1.5vh"}></AddIcon>Add A List
          </Heading>
        </Card>
      </PopoverTrigger>
      <Portal>
        <PopoverContent>
          <PopoverArrow />
          <PopoverHeader>Add A New List</PopoverHeader>
          <PopoverCloseButton />
          <PopoverBody>
            <Input
              colorScheme="blue"
              placeholder={"Enter the new list name..."}
              value={newListName === "New List" ? "" : newListName}
              onChange={storeNewListName}
            />
          </PopoverBody>
          <PopoverFooter>
            <Button onClick={createNewList}>Create List</Button>
          </PopoverFooter>
        </PopoverContent>
      </Portal>
    </Popover>
  );
}

export default AddAList;
