import { Card, Heading } from "@chakra-ui/react";
import React from "react";
import { Link } from "react-router-dom";

function EachBoard({index, boardImage, boardId, boardColor, boardName}) {
  let boardLink = `/board/${boardId}`;
  return (
    <Link key={index} to={boardLink}>
      <Card
        width={{ base: "90vw", md: "20vw" }}
        height={{ base: "20vh", md: "18vh" }}
        backgroundImage={boardImage}
        bg={boardColor}
        backgroundSize={"cover"}
        justify={"center"}
        align={"center"}
      >
        <Heading size="md">{boardName}</Heading>
      </Card>
    </Link>
  );
}

export default EachBoard;
