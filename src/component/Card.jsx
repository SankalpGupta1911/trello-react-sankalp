import React, { useState, useEffect } from "react";

import {
  getCheckListsInCard,
  createCheckList,
  deleteCheckList,
} from "../services/checkListService.cjs";
import {
  Button,
  Flex,
  Input,
  InputGroup,
  InputRightElement,
  useDisclosure,
} from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Heading,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import CheckList from "./CheckList.jsx";

function EachCard({ cardName, cardId, handleCardDelete }) {
  const [allCheckLists, setAllCheckLists] = useState([]);
  const [newCheckListName, setNewCheckListName] = useState(
    `New CheckList ${allCheckLists.length + 1}`
  );
  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    getCheckListsInCard(cardId).then((result) => {
      setAllCheckLists(result);
    });
  }, []);

  function updateNewCardName(event) {
    setNewCheckListName(
      event.target.value === ""
        ? `New CheckList ${allCheckLists.length + 1}`
        : event.target.value
    );
  }

  async function createNewCheckList() {
    const { checkListName, checkListId } = await createCheckList(
      cardId,
      newCheckListName
    );
    setAllCheckLists([
      ...allCheckLists,
      { checkListName: newCheckListName, checkListId: checkListId },
    ]);
    setNewCheckListName(`New CheckList ${allCheckLists.length + 2}`);
  }

  async function deleteCheckListFunc(checkListId) {
    let newArray = allCheckLists.filter(
      (checkList) => checkList.checkListId !== checkListId
    );
    setAllCheckLists(newArray);
    deleteCheckList(checkListId);
  }

  return (
    <>
      <Flex w={"100%"} alignItems={"center"}>
        <Button w={"90%"} onClick={onOpen} justifyContent={"space-around"}>
          {cardName}
        </Button>
        <DeleteIcon
          h={"100%"}
          margin={"0 8px 0 8px"}
          onClick={() => handleCardDelete(cardId)}
        ></DeleteIcon>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{cardName}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {allCheckLists.length > 0
              ? allCheckLists.map((checkList) => {
                  return (
                    <Flex
                      key={checkList.checkListId}
                      w={"100%"}
                      alignItems={"center"}
                      cursor="pointer"
                    >
                      <CheckList
                        cardId={cardId}
                        checkListName={checkList.checkListName}
                        checkListId={checkList.checkListId}
                      />
                      <DeleteIcon
                        ml={"20px"}
                        onClick={() =>
                          deleteCheckListFunc(checkList.checkListId)
                        }
                      />
                    </Flex>
                  );
                })
              : null}
          </ModalBody>

          <ModalFooter>
            <InputGroup p={"0"}>
              <Input
                p={"0"}
                placeholder="  Add A Checklist"
                onChange={updateNewCardName}
              />
              <InputRightElement width="4.5rem">
                <Button
                  h="1.75rem"
                  width={"fit-content"}
                  colorScheme="blue"
                  mr={3}
                  onClick={createNewCheckList}
                >
                  Add
                </Button>
              </InputRightElement>
            </InputGroup>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default EachCard;
