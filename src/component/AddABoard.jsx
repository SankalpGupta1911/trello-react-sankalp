import React from "react";

import { AddIcon, ChevronDownIcon } from "@chakra-ui/icons";
import {
  Button,
  Card,
  CardHeader,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";

function AddABoard(props) {
  const {
    isOpen,
    onOpen,
    onClose,
    initialRef,
    finalRef,
    updateNewCardName,
    AddACard,
    updateNewCardColor,
  } = props;

  return (
    <>
      <Card
        bg={"whitesmoke"}
        width={{ base: "90vw", md: "20vw" }}
        height={{ base: "20vh", md: "18vh" }}
        justify={"center"}
        align={"center"}
        onClick={onOpen}
      >
        <CardHeader
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Heading size="md">Add a board</Heading>
          <AddIcon></AddIcon>
        </CardHeader>
      </Card>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create a new board</ModalHeader>
          <ModalCloseButton />
          <ModalBody
            pb={6}
            display={"flex"}
            flexDirection={"column"}
            gap={"15%"}
          >
            <FormControl>
              <FormLabel>Board name</FormLabel>
              <Input
                ref={initialRef}
                onChange={updateNewCardName}
                placeholder="Enter a new board name"
              />
            </FormControl>

            <Menu>
              <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
                Select a color
              </MenuButton>
              <MenuList onClick={updateNewCardColor}>
                <MenuItem>blue</MenuItem>
                <MenuItem>orange</MenuItem>
                <MenuItem>green</MenuItem>
                <MenuItem>red</MenuItem>
                <MenuItem>purple</MenuItem>
                <MenuItem>pink</MenuItem>
                <MenuItem>lime</MenuItem>
                <MenuItem>grey</MenuItem>
              </MenuList>
            </Menu>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="blue"
              mr={3}
              onClick={() => AddACard()}
            >
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default AddABoard;
