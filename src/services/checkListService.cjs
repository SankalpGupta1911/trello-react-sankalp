import axios from "axios";

import { baseURL, key, token } from "../assets/ApiKey&Token.cjs";

export async function getCheckListsInCard(cardId) {
  try {
    const res = await axios.get(
      `${baseURL}/1/card/${cardId}/checklists/?key=${key}&token=${token}`
    );
    return res.data.reduce((acc, checklists) => {
      acc.push({
        checkListName: checklists.name,
        checkListId: checklists.id,
      });
      return acc;
    }, []);
  } catch (err) {
    return { error: err };
  }
}

export async function createCheckList(cardId, checkListName) {
  try {
    const res = await axios.post(`${baseURL}/1/checklists`, {
      name: checkListName,
      idCard: cardId,
      pos: "bottom",
      key: key,
      token: token,
    });
    return {
      checkListName: res.data.name,
      checkListId: res.data.id,
    };
  } catch (err) {
    return { error: err };
  }
}

export async function deleteCheckList(checkListId) {
  try {
    const res = await axios.delete(
      `${baseURL}/1/checklists/${checkListId}/?key=${key}&token=${token}`
    );
    return res.status;
  } catch (err) {
    return 500;
  }
}
