import axios from "axios";

import {baseURL,key,token} from "../assets/ApiKey&Token.cjs"

export async function getCardsInLists(ListId) {
  try {
    const res = await axios.get(
      `${baseURL}/1/list/${ListId}/cards?key=${key}&token=${token}`
    );
    return res.data.reduce((acc, card) => {
      acc.push({
        cardName: card.name,
        cardId: card.id,
      });
      return acc;
    }, []);
  } catch (err) {
    return { error: err };
  }
}

export async function createCard(listId, cardName) {
  try {
    const res = await axios.post(`${baseURL}/1/cards`, {
      name: cardName,
      idList: listId,
      pos: "bottom",
      key: key,
      token: token,
    });
    const card = await res.data;
    return {
      cardName: card.name,
      cardId: card.id,
    };
  } catch (err) {
    return { error: err };
  }
}

export async function deleteCard(cardID) {
    try {
      const res = await axios.delete(
        `${baseURL}/1/cards/${cardID}/?key=${key}&token=${token}`
      );
      return res.status;
    } catch (err) {
      return 500;
    }
  }