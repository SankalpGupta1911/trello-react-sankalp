import axios from "axios";

import {baseURL,key,token} from "../assets/ApiKey&Token.cjs"

export async function getBoards() {
  try {
    const res = await axios.get(
      `${baseURL}/1/members/me/boards?key=${key}&token=${token}`
    );
    return res.data.reduce((acc, board) => {
      acc.push({
        name: board.name,
        boardId: board.id,
        backgroundImg: board.prefs.backgroundImage,
        backgroundColor: board.prefs.background,
      });
      return acc;
    }, []);
  } catch (err) {
    return { error: err };
  }
}

export async function getBoardDetails(boardId) {
  try {
    const res = await axios.get(
      `${baseURL}/1/board/${boardId}?key=${key}&token=${token}`
    );
    const board = res.data;
    return {
      name: board.name,
      boardId: board.id,
      backgroundImg: board.prefs.backgroundImage,
    };
  } catch (err) {
    return { error: err };
  }
}

export async function createBoard(newBoardName, newBoardBackground) {
  try {
    const res = await axios.post(`${baseURL}/1/boards/`, {
      name: newBoardName,
      prefs_background: newBoardBackground,
      key: key,
      token: token,
    });
    const board = res.data;
    console.log(board);
    return {
      
      boardId: board.id,
      backgroundImg: board.prefs.backgroundImage,
    };
  } catch (err) {
    return { error: err };
  }
}
