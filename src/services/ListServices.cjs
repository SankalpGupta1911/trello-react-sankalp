import axios from "axios";

import {baseURL,key,token} from "../assets/ApiKey&Token.cjs"

export async function getLists(boardId) {
  try {
    const res = await axios.get(
      `${baseURL}/1/boards/${boardId}/lists?key=${key}&token=${token}`
    );
    return res.data.reduce((acc, list) => {
      acc.push({
        listName: list.name,
        listId: list.id,
      });
      return acc;
    }, []);
  } catch (err) {
    return { error: err };
  }
}

export async function createList(listName, boardId) {
  try {
    const res = await axios.post(`${baseURL}/1/lists`, {
      name: listName,
      idBoard: boardId,
      key: key,
      token: token,
      pos: "bottom",
    });
    const list = await res.data;
    return {
      listName: list.name,
      listId: list.id,
    };
  } catch (err) {
    return { error: err };
  }
}

export async function deleteList(listId) {
  try {
    const res = await axios.put(`${baseURL}/1/lists/${listId}/`, {
      closed: true,
      key: key,
      token: token,
    });
    return res.status;
  } catch (err) {
    return 500;
  }
}
