import React, { useState, useRef, useEffect } from "react";

import Board from "../component/Board";
import AddABoard from "../component/AddABoard";
import { getBoards, createBoard } from "../services/boardServices.cjs";

import { Flex, Heading, useDisclosure } from "@chakra-ui/react";
import { CircularProgress } from "@chakra-ui/react";

function AllBoards() {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [allBoards, setAllBoards] = useState([]);
  const [newCardName, setNewCardName] = useState(
    `New Card ${allBoards.length + 1}`
  );
  const [newCardColor, setNewCardColor] = useState("orange");

  useEffect(() => {
    getBoards()
      .then((result) => {
        setAllBoards(result);
        setLoading(false);
      })
      .catch((err) => setError(true));
  }, []);

  async function AddACard() {
    let { boardId, backgroundImg, newbackgroundColor } = await createBoard(
      newCardName,
      newCardColor
    );
    console.log(boardId);
    let newBoard = [
      ...allBoards,
      {
        name: newCardName,
        boardId: boardId,
        backgroundImg: backgroundImg,
        backgroundColor: newCardColor,
      },
    ];
    setAllBoards(newBoard);
    setNewCardName(`New Card ${allBoards.length + 2}`);
    setNewCardColor("orange");
    onClose();
  }

  function updateNewCardName(event) {
    setNewCardName(
      event.target.value === ""
        ? `New Card ${allBoards.length + 1}`
        : event.target.value
    );
  }

  function updateNewCardColor(event) {
    setNewCardColor(event.target.textContent.toLowerCase());
  }

  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = useRef(null);
  const finalRef = useRef(null);

  return (
    <>
      {error ? (
        <Heading>Some Error Fetching Data!</Heading>
      ) : loading ? (
        <Flex
          h={"100vh"}
          w={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          <CircularProgress
            isIndeterminate
            color="green.300"
          ></CircularProgress>
          <Heading>Loading boards...</Heading>
        </Flex>
      ) : (
        <Flex
          cursor={"pointer"}
          margin={{ base: "5vh 0 0 0", md: "10vh 5vw 0 5vw" }}
          gap={"2vw"}
          justify={{ base: "center", md: "flex-start" }}
          flexWrap={"wrap"}
        >
          {allBoards &&
            allBoards.map((board, i) => {
              return (
                <Board
                  key={i}
                  index={i}
                  boardImage={board.backgroundImg}
                  boardId={board.boardId}
                  boardName={board.name}
                  boardColor={board.backgroundColor}
                />
              );
            })}
          <AddABoard
            isOpen={isOpen}
            onOpen={onOpen}
            onClose={onClose}
            initialRef={initialRef}
            finalRef={finalRef}
            updateNewCardName={updateNewCardName}
            AddACard={AddACard}
            updateNewCardColor={updateNewCardColor}
            newCardName={newCardName}
            newCardColor={newCardColor}
          />
        </Flex>
      )}
    </>
  );
}

export default AllBoards;
