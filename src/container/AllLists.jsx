import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import { getLists, createList, deleteList } from "../services/ListServices.cjs";
import List from "../component/List.jsx";
import AddAList from "../component/AddAList.jsx";

import { Flex, Heading } from "@chakra-ui/react";
import { CircularProgress } from "@chakra-ui/react";

function AllLists() {
  const { boardId } = useParams();

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [newListName, setNewListName] = useState("New List");
  const [allLists, setAllLists] = useState([]);

  useEffect(() => {
    getLists(boardId)
      .then((result) => {
        setAllLists(result);
        setLoading(false);
      })
      .catch((err) => setError(true));
  }, []);

  async function createNewList() {
    const { listName, listId } = await createList(newListName, boardId);
    setAllLists([...allLists, { listName: newListName, listId: listId }]);
    setNewListName("New List");
  }

  function storeNewListName(event) {
    setNewListName(event.target.value === "" ? "New List" : event.target.value);
  }

  function deleteListFunc(listId) {
    let newArray = allLists.filter((list) => list.listId !== listId);
    setAllLists(newArray);
    deleteList(listId);
  }

  return (
    <>
      {error ? (
        <Heading>Some Error Fetching Data!</Heading>
      ) : loading ? (
        <Flex
          h={"100vh"}
          w={"100%"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          <CircularProgress
            isIndeterminate
            color="green.300"
          ></CircularProgress>
          <Heading>Loading Lists...</Heading>
        </Flex>
      ) : (
        <Flex bg={"blue"} flexDirection={"column"} width={"auto"}>
          <Flex
            bg={"whitesmoke"}
            justify={"flex-start"}
            alignItems={{ base: "center", md: "flex-start" }}
            flexDirection={{ base: "column", md: "row" }}
            w={"100%"}
            p={"1rem"}
            gap={"2vw"}
            overflowX={"scroll"}
          >
            {allLists &&
              allLists.map((list, i) => {
                return (
                  <List
                    key={i}
                    listName={list.listName}
                    listId={list.listId}
                    deleteListFunc={deleteListFunc}
                  />
                );
              })}
            <AddAList
              newListName={newListName}
              storeNewListName={storeNewListName}
              createNewList={createNewList}
            />
          </Flex>
        </Flex>
      )}
    </>
  );
}

export default AllLists;
