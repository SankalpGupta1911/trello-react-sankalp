import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import AllBoards from "./container/AllBoards";
import NavBar from "./component/NavBar";
import Error from "./component/Error";
import AllLists from "./container/AllLists";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/board" />} />
        <Route path="/board/*" element={<NavBar />}>
          <Route index element={<AllBoards />} />
          <Route path=":boardId" element={<AllLists />} />
        </Route>
        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
